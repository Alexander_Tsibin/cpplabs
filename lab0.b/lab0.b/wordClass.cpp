#include <string>
#include "wordClass.h"
namespace Alex {

	Word::Word(std::string str, int val)
	{
		word = str;
		value = val;
	};
	int Word::GetValue()
	{
		return value;
	};
	std::string Word::GetWord()
	{
		return word;
	};
	void Word::SetValue(int val)
	{
		value = val;
	};
	void Word::SetWord(std::string str)
	{
		word = str;
	};
	float Word::GetFrequency(int wordsAmount)
	{
		return (float(value) / float(wordsAmount));
	};

}