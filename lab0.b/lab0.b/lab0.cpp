﻿
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <algorithm>//for sort
#include <iomanip>// for setprecision
#include "ImportantFuncClass.h"
#include "wordClass.h"
#include "ReaderClass.h"
#define INPUT_FILE_NUM 1
#define OUTPUT_FILE_NUM 2
bool comp(Alex::Word a, Alex::Word b) {
	return a.GetValue() > b.GetValue();
}

int main(int argc, char* argv[])
{
	std::ifstream in(argv[INPUT_FILE_NUM]); // окрываем файл для чтения
	std::ofstream out(argv[OUTPUT_FILE_NUM]);
	if (!in.is_open())
	{
		return 1;
	}
	std::map< std::string, int> container;
	std::string line;
	Alex::Reader reader;
	Alex::ImportantFunc func;
	while (getline(in, line))
	{
		int start = 0;
		while (start < line.length())
		{
			reader.wordsAmount++;
			reader.SetLastWord(func.GetWord(line, start));
			//system("cls");
			//std::cout << line << std::endl;
			if (container.count(reader.GetLastWord()))
			{
				container[reader.GetLastWord()]++;
			}
			else
			{
				container.emplace(reader.GetLastWord(), 1);
			}
		}
	}
	std::map < std::string, int > ::iterator it;
	std::vector<Alex::Word> words;
	for (it = container.begin(); it != container.end(); it++)
	{
		words.push_back(Alex::Word(it->first, it->second));
	}
	std::sort(words.begin(), words.end(), comp);
	for (int i = 0; i < words.size(); i++)
	{
		out << words[i].GetWord() << ';' << words[i].GetValue();
		out << ';' << std::setprecision(3) << words[i].GetFrequency(reader.wordsAmount) * 100 << '%' << std::endl;
	}
	in.close();     // закрываем файл
	out.close();
	return 0;
}
