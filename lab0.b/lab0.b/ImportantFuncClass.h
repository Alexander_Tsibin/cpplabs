#pragma once
namespace Alex {
	class ImportantFunc
	{
	public:
		int DigitOrNumber(char symb);
		std::string GetWord(std::string str, int& start);
	};
}
