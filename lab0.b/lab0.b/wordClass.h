#pragma once
namespace Alex
{
	class Word {

	public:
		Word(std::string str, int val);
		int GetValue();
		std::string GetWord();
		void SetValue(int val);
		void SetWord(std::string str);
		float GetFrequency(int wordsAmount);
	private:
		std::string word;
		int value;

	};
}