#pragma once
namespace Alex {
	class Reader {
	private:
		std::string lastWord;
	public:
		int wordsAmount = 0;
		void SetLastWord(std::string str);
		std::string GetLastWord();

	};
}
