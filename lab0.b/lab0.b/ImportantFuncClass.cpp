#include <iostream>
#include <fstream>
#include <string>
#include <ctype.h>//for isdigit && isalpha
#include "ImportantFuncClass.h"
#define FIRST_LOWERCASE_LETTER_RUS '�'
#define LAST_LOWERCASE_LETTER_RUS '�'
#define FIRST_UPPERCASE_LETTER_RUS '�'
#define LAST_UPPERCASE_LETTER_RUS '�'
namespace Alex {

	int ImportantFunc::DigitOrNumber(char symb)
	{
		if (symb >= FIRST_LOWERCASE_LETTER_RUS && symb <= LAST_LOWERCASE_LETTER_RUS)
			return 1;
		if (symb >= FIRST_UPPERCASE_LETTER_RUS && symb <= LAST_UPPERCASE_LETTER_RUS)
			return 1;
		if (symb < 0)
			return 0;
		if (isdigit(symb) || isalpha(symb))
			return 1;

		return 0;
	}
	std::string ImportantFunc::GetWord(std::string str, int& start)
	{

		std::string word;
		int i = start;
		for (; DigitOrNumber(str[i]) && i < str.length(); i++)
		{
			word += str[i];
		}
		while (!DigitOrNumber(str[i]) && i < str.length())	i++;

		start = i;
		return word;
	}
}