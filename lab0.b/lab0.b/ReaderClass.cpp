#include <string>
#include "ReaderClass.h"
namespace Alex {
	void Reader::SetLastWord(std::string str)
	{
		lastWord = str;
	}
	std::string Reader::GetLastWord() {
		return lastWord;
	}
}