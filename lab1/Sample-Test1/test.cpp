#include "pch.h"
#include "../lab1/tritClass.h"
TEST(case1, TestName) {
    TritSet a;
    a[0] = Trit::True;
  EXPECT_TRUE(a[0] == Trit::True);
}
TEST(case2, TestName) {
    TritSet a;
    a[0] = Trit::True;
    a[0] = Trit::False;
    a[0] = Trit::Unknown;
    EXPECT_TRUE(a[0] == Trit::Unknown);
}
TEST(case3, TestName) {
    TritSet a(1000);
    size_t length = a.capacity();

    EXPECT_TRUE(length >= 16 * 2 / 8 / sizeof(unsigned int));
}
TEST(case4, TestName) {
    TritSet a;
    unsigned int length = a.capacity();
    a[1000] = Trit::Unknown;

    EXPECT_TRUE(length == a.capacity());
}
TEST(case5, TestName) {
    TritSet a;
    unsigned int length = a.capacity();
    if (a[20000] == Trit::True) {}

    EXPECT_TRUE(length == a.capacity());
}
TEST(case6, TestName) {
    TritSet a;
    unsigned int length = a.capacity();
    a[1000] = Trit::True;

    EXPECT_TRUE(length < a.capacity());
}
TEST(case7, TestName) {
    TritSet a;
    a[1000] = Trit::True;
    unsigned int length = a.capacity();
    a[1000000] = Trit::Unknown;
    a[100] = Trit::False;

    EXPECT_TRUE(length == a.capacity());
}
TEST(case8, TestName) {
    TritSet a;
    a[1000] = Trit::True;
    unsigned int length = a.capacity();
    a[1000000] = Trit::Unknown;
    a[100] = Trit::False;
    a.shrink();

    EXPECT_TRUE(length > a.capacity());
}
TEST(case9, TestName) {
    TritSet a(1000);
    a[0] = Trit::True;
    TritSet b(2000);
    b[0] = Trit::False;
    TritSet c = a & b;

    EXPECT_TRUE(c.capacity() == b.capacity());
}
TEST(case10, TestName) {
    TritSet a;
    a[0] = Trit::True;
    a[1] = Trit::True;
    a[5] = Trit::True;
    a[7] = Trit::True;
    a[8] = Trit::True;

    unsigned int amount = a.cardinality(Trit::True);

    EXPECT_TRUE(amount == 5);
}
TEST(case11, TestName) {
    TritSet a;
    a[0] = Trit::True;
    a[1] = Trit::True;
    a[5] = Trit::True;
    a[7] = Trit::True;
    a[8] = Trit::True;
    a[4] = Trit::True;

    unsigned int amount = a.cardinality(Trit::Unknown);

    EXPECT_TRUE(amount == 2);
}
TEST(case12, TestName) {
    TritSet a;
    a[0] = Trit::True;
    a[1] = Trit::True;
    a[4] = Trit::True;
    a[5] = Trit::True;
    a[7] = Trit::True;
    a[8] = Trit::True;

    std::unordered_map<Trit, int> map_test = a.cardinality();

    EXPECT_TRUE(map_test[Trit::False] == 0);
    EXPECT_TRUE(map_test[Trit::Unknown] == 3);
    EXPECT_TRUE(map_test[Trit::True] == 6);
}

TEST(case13, TestName) {
    TritSet a;
    a[0] = Trit::True;
    a[1] = Trit::True;
    a[5] = Trit::True;
    a[7] = Trit::True;
    a[8] = Trit::True;
    a[3] = Trit::True;
    a[2] = Trit::True;
    a[16] = Trit::True;

    a.trim(1);

    EXPECT_TRUE(a.capacity() == 1);
}

TEST(case14, TestName) {
    TritSet a;
    a[0] = Trit::True;
    a[1] = Trit::True;
    a[5] = Trit::True;
    a[7] = Trit::True;
    a[8] = Trit::True;
    a[3] = Trit::True;
    a[2] = Trit::True;
    a[16] = Trit::True;

    a.trim(3);

    EXPECT_TRUE(a[5] == Trit::Unknown);
}

TEST(case15, TestName) {
    TritSet a;
    a[0] = Trit::True;
    a[1] = Trit::True;
    a[5] = Trit::True;
    a[7] = Trit::True;
    a[8] = Trit::True;
    a[3] = Trit::True;
    a[2] = Trit::True;
    a[16] = Trit::True;

    unsigned int m = a.length();

    EXPECT_TRUE(m == 16);
}

TEST(case16, TestName) {
    TritSet a, b, c;
    a[0] = Trit::True;
    b[0] = Trit::False;

    c = a & b;

    EXPECT_TRUE(c[0] == Trit::False);
}

TEST(case17, TestName) {
    TritSet a, b, c;
    a[0] = Trit::True;
    a[0] = Trit::Unknown;
    b[0] = Trit::False;

    c = a | b;

    EXPECT_TRUE(c[0] == Trit::Unknown);
}

TEST(case18, TestName) {
    TritSet a, b;
    a[0] = Trit::True;
    b[0] = Trit::False;

    a &= b;

    EXPECT_TRUE(a[0] == Trit::False);
}


TEST(case19, TestName) {
    TritSet a, b;
    a[0] = Trit::True;
    a[0] = Trit::Unknown;
    b[0] = Trit::False;

    a |= b;

    EXPECT_TRUE(a[0] == Trit::Unknown);
}

TEST(case20, TestName) {
    TritSet a{ Trit::True, Trit::Unknown, Trit::False };

    EXPECT_TRUE(a[0] == Trit::True);
    EXPECT_TRUE(a[2] == Trit::False);
}

TEST(case21, TestName) {
    TritSet a{ Trit::True, Trit::Unknown, Trit::False };

    EXPECT_TRUE(a[0] == Trit::True);
    EXPECT_TRUE(a[2] == Trit::False);
}

TEST(case22, TestName) {
    std::stringstream test_cout;
    test_cout << Trit::True;

    EXPECT_TRUE(test_cout.str() == "True");
}

TEST(case23, TestName) {
    TritSet a{ Trit::True, Trit::Unknown, Trit::False };
    std::stringstream test_cout;
    test_cout << a;

    EXPECT_TRUE(test_cout.str() == "True Unknown False ");
}


