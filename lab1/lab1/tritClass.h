#pragma once
#include <iostream>
#include <unordered_map>
#include <sstream>

enum class Trit {
    False, Unknown, True
};

class TritSetProxy;

class TritSet {
    friend class TritSetProxy;

    unsigned int* array;
    unsigned int array_size = 0, last_index = 0, last_trit = 0;

public:
    TritSet() {};
    ~TritSet();
    TritSet(unsigned int m);
    TritSet(const std::initializer_list<Trit>& list);
    unsigned int capacity() {
        return array_size;
    }
    Trit operator[](unsigned int m) const;
    TritSet operator&(const TritSet& working_tritset) const;
    TritSet operator|(const TritSet& working_tritset) const;
    TritSet operator!() const;
    TritSet& operator&=(const TritSet& working_tritset);
    TritSet& operator|=(const TritSet& working_tritset);
    TritSet& operator=(const TritSet& working_tritset);
    TritSetProxy operator[](unsigned int m);
    std::unordered_map<Trit, int> cardinality() const;
    unsigned int cardinality(Trit value) const;
    void trim(unsigned int index);
    unsigned int length() const;
    void shrink();
};

class TritSetProxy {
    friend class TritSet;

    unsigned int array_index, trit_index;
    TritSet* pointer;
    void correct_array();
    TritSetProxy(unsigned int array_index_value, unsigned int trit_index_value, TritSet* pointer_value)
        : array_index(array_index_value), trit_index(trit_index_value), pointer(pointer_value) {}

public:
    TritSetProxy& operator=(Trit value);
    bool operator==(Trit value) const;
    void operator==(TritSetProxy value) const {}
};

std::ostream& operator<<(std::ostream& new_os, TritSet& working_tritset);
std::ostream& operator<<(std::ostream& new_os, const Trit& value);
Trit operator!(const Trit& a);
Trit operator|(const Trit& a, const Trit& b);
Trit operator&(const Trit& a, const Trit& b);