#include "tritClass.h"

TritSet::TritSet(unsigned int m) {
    if (m != 0) {
        if ((m - 1) / (sizeof(unsigned int) * 4) + 1 > array_size) {
            if (array_size) {
                unsigned int* new_array = new unsigned int[(m - 1) / (sizeof(unsigned int) * 4) + 1];
                for (int i = 0; i < array_size; i++) {
                    new_array[i] = array[i];
                }
                array = new_array;
                array_size = m / (sizeof(unsigned int) * 4);
            }
            else {
                unsigned int* new_array = new unsigned int[(m - 1) / (sizeof(unsigned int) * 4) + 1];
                for (int i = 0; i < (m - 1) / (sizeof(unsigned int) * 4) + 1; i++) {
                    new_array[i] = 0;
                }
                array = new_array;
                array_size = (m - 1) / (sizeof(unsigned int) * 4) + 1;
            }
        }
        else if (array == nullptr) {
            unsigned int* new_array = new unsigned int[1];
            new_array[0] = 0;
            array = new_array;
        }
    }
}

TritSet::~TritSet() {
    if (array_size)
        delete[] array;
}

void TritSet::shrink() {
    unsigned int* new_array = new unsigned int[last_index + 1];
    for (int i = 0; i < last_index + 1; i++) {
        new_array[i] = 0;
    }
    if (array == nullptr) {
        new_array[0] = 0;
        array = new_array;
        array_size = 1;
        return;
    }
    for (int i = 0; i < last_index + 1; i++) {
        new_array[i] = array[i];
    }
    array = new_array;
    array_size = last_index + 1;
}

Trit TritSet::operator[](unsigned int m) const {
    unsigned int array_index = m / (sizeof(unsigned int) * 4);
    unsigned int trit_index = m % (sizeof(unsigned int) * 4);
    if (array_index >= array_size)
        return Trit::Unknown;

    unsigned int the_one = array[array_index];
    unsigned int rep_amount = sizeof(unsigned int) * 4 - 1 - trit_index;
    for (unsigned int i = 0; i < rep_amount; i++) {
        the_one >>= 2;
    }
    the_one &= 3;
    switch (the_one) {
    case 0:
        return Trit::Unknown;
    case 1:
        return Trit::True;
    case 2:
        return Trit::False;
    default:
        delete[] array;
        exit(1);
    }
}

TritSet& TritSet::operator=(const TritSet& working_tritset) {
    if (this == &working_tritset) {
        return (*this);
    }
    unsigned int* new_array = new unsigned int[working_tritset.array_size];
    for (int i = 0; i < working_tritset.array_size; i++) {
        new_array[i] = working_tritset.array[i];
    }
    if (this->array_size != 0)
        delete[] this->array;

    this->array = new_array;
    this->last_index = working_tritset.last_index;
    this->array_size = working_tritset.array_size;

    return (*this);
}

void TritSet::trim(unsigned int index) {
    for (unsigned int i = index; i < array_size * sizeof(unsigned int) * 4; i++) {
        (*this)[i] = Trit::Unknown;
    }

    unsigned int* new_array = new unsigned int[index / (sizeof(unsigned int) * 4) + 1];
    for (int i = 0; i < index / (sizeof(unsigned int) * 4) + 1; i++) {
        new_array[i] = array[i];
    }
    array = new_array;
    array_size = index / (sizeof(unsigned int) * 4) + 1;
}


TritSetProxy TritSet::operator[](unsigned int m) {
    unsigned int array_index = m / (sizeof(unsigned int) * 4);
    unsigned int trit_index = m % (sizeof(unsigned int) * 4);
    TritSetProxy value(array_index, trit_index, this);

    return value;
}

TritSet TritSet::operator&(const TritSet& working_tritset) const {
    TritSet result, base;

    if (this->array_size >= working_tritset.array_size) {
        result = (*this);
        base = working_tritset;
    }
    else {
        result = working_tritset;
        base = (*this);
    }
    for (int i = 0; i < base.array_size * sizeof(unsigned int) * 8; i += 2) {
        if (base[i / 2] == Trit::False) {
            result[i / 2] = Trit::False;
        }
        else if (base[i / 2] == Trit::Unknown) {
            if (result[i / 2] == Trit::True) {
                result[i / 2] = Trit::Unknown;
            }
        }
    }
    for (int i = base.array_size * sizeof(unsigned int) * 8; i < result.array_size * sizeof(unsigned int) * 8; i += 2) {
        if (result[i / 2] == Trit::True)
            result[i / 2] = Trit::Unknown;
    }

    return result;
}

TritSet& TritSet::operator&=(const TritSet& working_tritset) {
    TritSet temp = ((*this) & working_tritset);
    *this = temp;
    return *this;
}

TritSet TritSet::operator|(const TritSet& working_tritset) const {
    TritSet result, base;

    if (this->array_size >= working_tritset.array_size) {
        result = (*this);
        base = working_tritset;
    }
    else {
        result = working_tritset;
        base = (*this);
    }
    for (int i = 0; i < base.array_size * sizeof(unsigned int) * 8; i += 2) {
        if (base[i / 2] == Trit::True) {
            result[i / 2] = Trit::True;
        }
        else if (base[i / 2] == Trit::Unknown) {
            if (result[i / 2] == Trit::False) {
                result[i / 2] = Trit::Unknown;
            }
        }
    }
    for (int i = base.array_size * sizeof(unsigned int) * 8; i < result.array_size * sizeof(unsigned int) * 8; i += 2) {
        if (result[i / 2] == Trit::False)
            result[i / 2] = Trit::Unknown;
    }

    return result;
}

TritSet& TritSet::operator|=(const TritSet& working_tritset) {
    TritSet temp = ((*this) | working_tritset);
    *this = temp;
    return *this;
}

TritSet TritSet::operator!() const {
    TritSet base;

    base.array_size = this->array_size;
    base.last_index = this->last_index;
    unsigned int* new_array = new unsigned int[this->array_size];
    for (int i = 0; i < this->array_size; i++) {
        new_array[i] = this->array[i];
    }
    base.array = new_array;
    for (int i = 0; i < base.array_size * sizeof(unsigned int) * 8; i += 2) {
        if (base[i / 2] == Trit::True) {
            base[i / 2] = Trit::False;
        }
        else if (base[i / 2] == Trit::False) {
            base[i / 2] = Trit::True;
        }
    }

    return base;
}

unsigned int TritSet::cardinality(Trit value) const //����� ������������� � ������ �������� ������
{
    unsigned int m = 0;
    if (value != Trit::Unknown) {
        for (int i = 0; i < array_size * sizeof(unsigned int) * 4; i++) {
            if ((*this)[i] == value) {
                m++;
            }
        }
    }
    else {
        for (int i = 0; i <= last_trit; i++) {
            if ((*this)[i] == value) {
                m++;
            }
        }
    }

    return m;
}

std::unordered_map<Trit, int> TritSet::cardinality() const //����� ������������� � ������ �������� ������ ��� ���� ����� ������
{
    std::unordered_map<Trit, int> trits_map;

    unsigned int false_amount = 0, unknown_amount = 0, true_amount = 0;

    for (int i = 0; i <= last_trit; i++) {
        if ((*this)[i] == Trit::Unknown) {
            unknown_amount++;
        }
        else if ((*this)[i] == Trit::True) {
            true_amount++;
        }
        else false_amount++;
    }

    for (int i = last_trit + 1; i < array_size * sizeof(unsigned int) * 4; i++) {
        if ((*this)[i] == Trit::True) {
            true_amount++;
        }
        else if ((*this)[i] == Trit::False) {
            false_amount++;
        }
    }

    trits_map[Trit::False] = false_amount;
    trits_map[Trit::Unknown] = unknown_amount;
    trits_map[Trit::True] = true_amount;

    return trits_map;
}

unsigned int TritSet::length() const {
    unsigned int m = 0;
    for (unsigned int i = 0; i < array_size * sizeof(unsigned int) * 4; i++) {
        if (((*this)[i] == Trit::True) || ((*this)[i] == Trit::False)) {
            m = i;
        }
    }

    return m;
}

Trit operator&(const Trit& a, const Trit& b) {
    if ((a == Trit::False) || (b == Trit::False))
        return Trit::False;
    if ((a == Trit::True) && (b == Trit::True))
        return Trit::True;
    return Trit::Unknown;
}

Trit operator|(const Trit& a, const Trit& b) {
    if ((a == Trit::False) && (b == Trit::False))
        return Trit::False;
    if ((a == Trit::True) || (b == Trit::True))
        return Trit::True;
    return Trit::Unknown;
}

Trit operator!(const Trit& a) {
    if (a == Trit::True)
        return Trit::False;
    if (a == Trit::False)
        return Trit::True;
    return Trit::Unknown;
}

std::ostream& operator<<(std::ostream& new_os, const Trit& value) {
    switch (value) {
    case Trit::False:
        new_os << "False";
        break;
    case Trit::Unknown:
        new_os << "Unknown";
        break;
    case Trit::True:
        new_os << "True";
        break;
    default:
        new_os << "";
        break;
    }

    return new_os;
}

std::ostream& operator<<(std::ostream& new_os, TritSet& working_tritset) {
    for (unsigned int i = 0; i <= working_tritset.length(); i++)
        if (working_tritset[i] == Trit::False) {
            new_os << Trit::False << ' ';
        }
        else if (working_tritset[i] == Trit::Unknown) {
            new_os << Trit::Unknown << ' ';
        }
        else new_os << Trit::True << ' ';

    return new_os;
}

TritSet::TritSet(const std::initializer_list<Trit>& list) {
    TritSet(list.size());
    {
        int count = 0;
        int m = 0;
        for (auto& element : list) {
            operator[](m) = element;
            //            operator=(element);
            m++;
        }
    };
}

void TritSetProxy::correct_array() {
    if (array_index >= pointer->array_size) {
        if (pointer->array_size) {
            unsigned int* new_array = new unsigned int[array_index + 1];
            for (int i = 0; i < pointer->array_size; i++) {
                new_array[i] = pointer->array[i];
            }
            for (int i = pointer->array_size; i < array_index + 1; i++) {
                new_array[i] = 0;
            }
            pointer->array = new_array;
            pointer->array_size = array_index + 1;
        }
        else {
            unsigned int* new_array = new unsigned int[array_index + 1];
            for (int i = 0; i < array_index + 1; i++) {
                new_array[i] = 0;
            }

            pointer->array = new_array;
            pointer->array_size = array_index + 1;
        }
    }
}

TritSetProxy& TritSetProxy::operator=(Trit value) {
    unsigned int empty_one = 0;

    switch (value) {
    case Trit::True:
        empty_one |= (1 << (sizeof(unsigned int) * 8 - 1 - trit_index * 2));
        empty_one |= (1 << (sizeof(unsigned int) * 8 - 2 - trit_index * 2));

        correct_array();
        pointer->array[array_index] &= ~empty_one;

        empty_one = 0;
        empty_one |= (1 << (sizeof(unsigned int) * 8 - 2 - trit_index * 2));
        pointer->array[array_index] |= empty_one;
        pointer->last_index = array_index;
        pointer->last_trit = trit_index;
        break;
    case Trit::False:
        empty_one = 0;

        empty_one |= (1 << (sizeof(unsigned int) * 8 - 1 - trit_index * 2));
        empty_one |= (1 << (sizeof(unsigned int) * 8 - 2 - trit_index * 2));

        correct_array();
        pointer->array[array_index] &= ~empty_one;

        empty_one = 0;
        empty_one |= (1 << (sizeof(unsigned int) * 8 - 1 - trit_index * 2));
        pointer->array[array_index] |= empty_one;
        pointer->last_index = array_index;
        pointer->last_trit = trit_index;
        break;
    case Trit::Unknown:
        empty_one = 0;

        empty_one |= (1 << (sizeof(unsigned int) * 8 - 1 - trit_index * 2));
        empty_one |= (1 << (sizeof(unsigned int) * 8 - 2 - trit_index * 2));

        if (array_index < pointer->array_size) {
            correct_array();
            pointer->array[array_index] &= ~empty_one;
            pointer->last_index = array_index;
            pointer->last_trit = trit_index;
        }
        break;
    default:
        delete[] pointer->array;
        exit(1);
    }

    return *this;
}

bool TritSetProxy::operator==(Trit value) const {
    if (array_index < pointer->array_size) {
        unsigned int the_one = pointer->array[array_index];
        unsigned int rep_amount = sizeof(unsigned int) * 4 - 1 - trit_index;
        for (unsigned int i = 0; i < rep_amount; i++) {
            the_one >>= 2;
        }
        the_one &= 3;
        switch (the_one) {
        case 2:
            return (Trit::False == value);
        case 1:
            return (Trit::True == value);
        case 0:
            return (Trit::Unknown == value);
        default:
            delete[] pointer->array;
            exit(1);
        }
    }
    else
        return false;
}
