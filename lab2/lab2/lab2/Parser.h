#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>
#include <ctype.h>
struct CommandData {
	std::string command;
	int command_num;
	std::vector<std::string> arguments;
};
class Parser
{private:
	std::vector<CommandData> _commands_data;
	std::vector<int> _commands_order;
	std::vector<std::string> _available_commands = { "readfile", "writefile", "grep", "sort", "replace", "dump" };
	std::string CutFirstWord(std::string& full_str);
	void SetCommandArguments(CommandData& command_data, int args_amount, std::string& str);
	void SetCommandData(const std::string& full_str, CommandData& command_data);
	int ConvertStrInInt(const std::string& str);
	void SetOrder(const std::string& str);
	void CheckOrderCorrectness(int command_num, bool& input_stream_is_open);
public:
	void ParseFile(const std::string& file_name);
	std::vector<int> GetCommandsOrder() const;
	std::vector<CommandData> GetCommandsData() const;

};

