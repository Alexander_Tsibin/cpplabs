#include "Sort.h"
#include "..\Creators.h"

namespace {
    //Global function
    bool InitializeSortCommand() {
        CommandFactory::GetInstance().RegisterCommand("sort", new SortCreator);
        return true;
    }
    //Calling the global function
    bool state = InitializeSortCommand();
}


std::vector<std::string> Sort::operation(const std::vector<std::string>& args, const std::vector<std::string>& text)
{
	std::vector<std::string> my_vector(text);

	std::sort(my_vector.begin(), my_vector.end(), compare_strings);
	return my_vector;
}
