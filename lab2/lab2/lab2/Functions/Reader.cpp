#include "Reader.h"
#include "..\Creators.h"

namespace {
    //Global function
    bool InitializeReaderCommand() {
        CommandFactory::GetInstance().RegisterCommand("readfile", new ReaderCreator);
        return true;
    }
    //Calling the global function
    bool state = InitializeReaderCommand();
}

std::vector<std::string> Reader::operation(const std::vector<std::string>& args, const std::vector<std::string>& text) {

	std::string file_name(args[0]);
	std::ifstream file(file_name);

	try {
		if (!file.is_open())
			throw std::exception("Error opening file!");
	}
	catch (char* str) {
		std::cout << str << std::endl;
	}

	std::vector<std::string> new_text;
	std::string buffer = "";

	while (!file.eof()) {
		std::getline(file, buffer);
		new_text.push_back(buffer);
	}

	file.close();
	return new_text;
}
