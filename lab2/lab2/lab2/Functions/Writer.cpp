#include "Writer.h"
#include "..\Creators.h"

namespace {
    //Global function
    bool InitializeWriterCommand() {
        CommandFactory::GetInstance().RegisterCommand("writefile", new WriterCreator);
        return true;
    }
    //Calling the global function
    bool state = InitializeWriterCommand();
}



std::vector<std::string> Writer::operation(const std::vector<std::string>& args, const std::vector<std::string>& text) {

	std::vector<std::string> my_vector(text);
	std::ofstream file(args[0]);

	try {
		if (!file.is_open())
			throw "Error opening file!";
	}
	catch (char* str) {
		std::cout << str << std::endl;
	}

	for (size_t it = 0; it != my_vector.size(); ++it)
		file << my_vector[it] << "\n";

	file.close();
	return my_vector;
}

