#include "Parser.h"

#define SPACE ' '
#define LINEFEED '\n'

std::vector<CommandData> Parser::GetCommandsData() const
{ 
	return _commands_data; 
}

std::vector<int> Parser::GetCommandsOrder() const
{
	return _commands_order;
}


std::string Parser::CutFirstWord(std::string& full_str)
{
	std::string str = "";
	while (full_str.length() != 0 && (full_str[0] == SPACE || full_str[0] == LINEFEED))//skip spaces and linefeeds before word
	{
		full_str.erase(0, 1);
	}
	while (full_str.length() != 0 && full_str[0] != SPACE && full_str[0] != LINEFEED)
	{
		str += full_str[0];
		full_str.erase(0, 1);
	}
	return str;
}

int Parser::ConvertStrInInt(const std::string& str)
{
	int ret_value = 0;
	for (int i = 0; i < str.length(); i++)
	{
		if (isdigit(str[i]))
		{
			ret_value *= 10;
			ret_value += (str[i] - '0');
		}
		else
		{
			std::cout << "wrong number: " << str << std::endl;
			throw std::exception("write correct number");
		}
	}
	return ret_value;
}

void Parser::SetCommandArguments(CommandData& command_data, int args_amount, std::string& str)
{
	command_data.arguments.resize(args_amount);
	for (int i = 0; i < args_amount; i++)
	{
		command_data.arguments[i] = CutFirstWord(str);
	}

	if (str != "")
	{
		std::cout << "extra argumets: " << str << std::endl;
		throw std::exception("delete extra arguments");
	}

}

void Parser::SetCommandData(const std::string& full_str, CommandData& command_data)
{
	std::string str_copy = full_str;
	command_data.command_num = ConvertStrInInt(CutFirstWord(str_copy));
	if (CutFirstWord(str_copy) != "=")
	{
		std::cout << "no '=' symb in string: " << full_str << std::endl;
		throw std::exception("no symb \"=\"");
	}
	std::string command_name = CutFirstWord(str_copy);
	command_data.command = command_name;
	
	/*readfile - 0
	* writefile - 1
	* grep - 2
	* sort - 3
	* replace - 4
	* dump -5
	*/
	int i = 0;
	for (; i < _available_commands.size(); i++)
	{
		if (command_name == _available_commands[i])
		{
			break;
		}
	}
	switch (i)
	{
		int args_amount;
	case(0)://readfile - 0
	{
		int args_amount = 1;
		SetCommandArguments(command_data, args_amount, str_copy);
		break;
	}
	case(1)://writefile - 1
	{
		int args_amount = 1;
		SetCommandArguments(command_data, args_amount, str_copy);
		break;
	}
	case(2)://grep - 2
	{
		int args_amount = 1;
		SetCommandArguments(command_data, args_amount, str_copy);
		break;
	}
	case(3)://sort - 3
	{
		int args_amount = 0;
		SetCommandArguments(command_data, args_amount, str_copy);
		break;
	}
	case(4)://replace - 4
	{
		int args_amount = 2;
		SetCommandArguments(command_data, args_amount, str_copy);
		break;
	}
	case(5)://dump - 5
	{
		int args_amount = 1;
		SetCommandArguments(command_data, args_amount, str_copy);
		break;
	}
	default:
		std::cout << "command \"" << command_name << "\" don't exist" << std::endl;
		throw std::exception("fix command name");
	}

}

void Parser::CheckOrderCorrectness(int command_num, bool& input_stream_is_open)
{
	std::string command_name = "";
	for (int i = 0; i < _commands_data.size(); i++)
	{
		if (_commands_data[i].command_num == command_num)
		{
			command_name = _commands_data[i].command;
			break;
		}
	}
	if (command_name == "")
	{
		std::cout << "no command with number " << command_num << std::endl;
		throw std::exception("fix command number in schema structure description");
	}
	int i = 0;
	for (; i < _available_commands.size(); i++)
	{
		if (command_name == _available_commands[i])
		{
			break;
		}
	}
	switch (i)
	{
	case(0)://readfile - 0
	{
		if (input_stream_is_open)
		{
			std::cout << "there is open input stream before \"" << command_name << "\" with num: " << command_num << std::endl;
			throw std::exception("fix commands order in schema structure description");
		}
		input_stream_is_open = true;
		break;
	}
	case(1)://writefile - 1
	{
		if (!input_stream_is_open)
		{
			std::cout << "there is no open input stream before \"" << command_name << "\" with num: " << command_num << std::endl;
			throw std::exception("fix commands order in schema structure description");
		}
		input_stream_is_open = false;
		break;
	}
	case(2)://grep - 2
	{
		if (!input_stream_is_open)
		{
			std::cout << "there is no open input stream before \"" << command_name << "\" with num: " << command_num << std::endl;
			throw std::exception("fix commands order in schema structure description");
		}
		input_stream_is_open = true;
		break;
	}
	case(3)://sort - 3
	{
		if (!input_stream_is_open)
		{
			std::cout << "there is no open input stream before \"" << command_name << "\" with num: " << command_num << std::endl;
			throw std::exception("fix commands order in schema structure description");
		}
		input_stream_is_open = true;
		break;
	}
	case(4)://replace - 4
	{
		if (!input_stream_is_open)
		{
			std::cout << "there is no open input stream before \"" << command_name << "\" with num: " << command_num << std::endl;
			throw std::exception("fix commands order in schema structure description");
		}
		input_stream_is_open = true;
		break;
	}
	case(5)://dump - 5
	{
		if (!input_stream_is_open)
		{
			std::cout << "there is no open input stream before \"" << command_name << "\" with num: " << command_num << std::endl;
			throw std::exception("fix commands order in schema structure description");
		}
		input_stream_is_open = true;
		break;
	}
	default:
		throw std::exception("programmer made a mistake with available commands, please don't blame him");
	}
	return;
}
void DeleteSpacesInEnd(std::string& str)
{
	while (str[str.length() - 1] == SPACE && str.length() > 0)
		str.resize(str.length() - 1);
}
void Parser::SetOrder(const std::string& str)
{
	std::string str_copy = str;
	DeleteSpacesInEnd(str_copy);
	bool input_stream_is_open = false;
	int i = 0;
	while (str_copy != "")
	{
		int command_num = ConvertStrInInt(CutFirstWord(str_copy));
		CheckOrderCorrectness(command_num, input_stream_is_open);
		if (CutFirstWord(str_copy) != "->" && str_copy != "")
		{
			std::cout << "no \"->\" in schema structure description after command: " << command_num << std::endl;
			throw std::exception("add \"->\"");
		}
		_commands_order.resize(i + 1);
		_commands_order[i] = command_num;
		i++;
	}

}

void Parser::ParseFile(const std::string& file_name) {
	std::ifstream in(file_name);
	if (!in.is_open())
	{
		throw std::exception("can't open file");
	}
	std::string str;
	getline(in, str);
	if (CutFirstWord(str) != "desc")
	{
		throw std::exception("no desc at the begining");
	}
	std::string str_copy;
	bool check_csed = false;
	for (int i = 0; getline(in, str); i++)
	{
		str_copy = str;
		if (CutFirstWord(str_copy) == "csed")
		{
			check_csed = true;
			break;
		}
		_commands_data.resize(i + 1);
		SetCommandData(str, _commands_data[i]);
		i++;
	}
	if (!check_csed)
	{
		throw std::exception("no csed");
	}
	if (getline(in, str))
	{
		SetOrder(str);
	}
	else
	{
		throw std::exception("no schema structure description");
	}
	in.close();
}
