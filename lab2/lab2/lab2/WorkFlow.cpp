#include "WorkFlow.h"

#include "CommandFactory.h"


void WorkFlow::session(const std::string& file_name) {
	Parser parser;
	try
	{
		parser.ParseFile(file_name);

	}
	catch (const std::exception& ex)
	{
		std::cout << ex.what() << std::endl;
		return;
	}
	auto commands_order(parser.GetCommandsOrder());
	auto commands_data(parser.GetCommandsData());
	CommandFactory factory = CommandFactory::GetInstance();
	std::vector<std::string> result;

	for (int i = 0; i < commands_order.size(); i++)
	{
		for (int j = 0; j < commands_data.size(); j++)
		{
			if (commands_order[i] == commands_data[j].command_num)
			{
				auto block = factory.GetCommand(commands_data[j].command);
				try
				{
					result = block->operation(commands_data[j].arguments, result);

				}
				catch (const std::exception& ex)
				{
					std::cout << ex.what() << std::endl;
				}
				delete block;
			}
		}
	}

	return;
}
