﻿
#include <iostream>
#include <vector>
#include "WorkFlow.h"

int main()
{
	WorkFlow workflow;
	try
	{
		workflow.session("workflow.txt");

	}
	catch (const std::exception& ex)
	{
		std::cout << ex.what() << std::endl;
		return 1;
	}
	return 0;
}

