#pragma once
#include "IBlockCreator.h"
#include "AllFunctionsHeaders.h"
#include "CommandFactory.h"
class DumpCreator : public IBlockCreator
{
	IBlock* CreateBlock() const override
	{
		return new Dump;
	}
};

class SortCreator : public IBlockCreator
{
	IBlock* CreateBlock() const override
	{
		return new Sort;
	}
};

class ReplaceCreator : public IBlockCreator
{
	IBlock* CreateBlock() const override
	{
		return new Replace;
	}
};


class GrepCreator : public IBlockCreator
{
	IBlock* CreateBlock() const override
	{
		return new Grep;
	}
};


class WriterCreator : public IBlockCreator
{
	IBlock* CreateBlock() const override
	{
		return new Writer;
	}
};


class ReaderCreator : public IBlockCreator
{
	IBlock* CreateBlock() const override
	{
		return new Reader;
	}
};