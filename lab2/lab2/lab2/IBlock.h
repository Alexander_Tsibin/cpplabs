#pragma once
#include <string>
#include <vector>
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <map>
class IBlock
{
public:
	virtual ~IBlock() {}
	virtual std::vector<std::string> operation(const std::vector<std::string>& args,const std::vector<std::string>& text) = 0;
};

