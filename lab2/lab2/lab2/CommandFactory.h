#pragma once
#include "IBlockCreator.h"
#include "IBlock.h"
#include <map>
#include <string>
class CommandFactory
{
private:
	std::map < std::string, IBlockCreator*> _commands;
public:
	static CommandFactory& GetInstance()
	{
		static CommandFactory factory;
		return factory;
	}

    void RegisterCommand(const std::string& name, IBlockCreator* creator) {
        auto it = _commands.find(name);
        if (it != _commands.end()) {
            if (it->second == creator) return;
            else delete it->second;
        }
        _commands[name] = creator;
    }
    IBlock* GetCommand(const std::string& name) {
        auto it = _commands.find(name);
        if (it == _commands.end()) {
            throw std::exception ("Command not registered yet");
        }
        return  it->second->CreateBlock();
    }
};