#pragma once
#include "IBlock.h"

class IBlockCreator
{
public:
	virtual ~IBlockCreator() {}
	virtual IBlock* CreateBlock() const = 0;

};